package com.example.mypc.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DataShowActivity extends AppCompatActivity {

    TextView Showdata, Showdata2, Showdata3, Showdata4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_show);

        getSupportActionBar().setTitle("Show Data Here...");


        Showdata = findViewById(R.id.showdata);
        Showdata2 = findViewById(R.id.showdata2);
        Showdata3 = findViewById(R.id.showdata3);
        Showdata4 = findViewById(R.id.showdata4);

        Intent i = getIntent();
        String s1 = i.getStringExtra("value");
        String s2 = i.getStringExtra("value2");
        String s3 = i.getStringExtra("value3");
        String s4 = i.getStringExtra("value4");
        Showdata.setText(s1);
        Showdata2.setText(s2);
        Showdata3.setText(s3);
        Showdata4.setText(s4);
    }

}
