package com.example.mypc.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {




    Button registerbutton;
    EditText firstname, lastname, gmail, mobileno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getSupportActionBar().setTitle("Registration Form");

        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        gmail = (EditText) findViewById(R.id.gmail);
        mobileno = (EditText) findViewById(R.id.mobileno);


        registerbutton =(Button) findViewById(R.id.registerbutton);






        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (firstname.length() == 0)
                {
                    firstname.setError("Enter Username");
                }
                else if (lastname.length() == 0)
                {
                    lastname.setError("Enter Password");
                }
                else if (mobileno.length() ==0)
                {
                    mobileno.setError("Enter Mobile No.");
                }
                else if (gmail.length() ==0)
                {
                    gmail.setError("Enter Gmail");
                }
                else
                {

                    String s1 = firstname.getText().toString();
                    String s2 = lastname.getText().toString();
                    String s3 = gmail.getText().toString();
                    String s4 = mobileno.getText().toString();

                    Intent i = new Intent(getApplicationContext(),DataShowActivity.class);
                    i.putExtra("value",s1);
                    i.putExtra("value2",s2);
                    i.putExtra("value3",s3);
                    i.putExtra("value4",s4);
                    startActivity(i);

                    Toast.makeText(RegistrationActivity.this, "Registered Successfully!!", Toast.LENGTH_SHORT).show();
                }


            }
        });




        }
}
